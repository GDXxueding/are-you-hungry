// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import goods from './components/goods/goods.vue';
import ratings from './components/ratings/ratings.vue';
import seller from './components/seller/seller.vue';
import detail from './components/detail/detail.vue';
import axios from "axios";
import qs from 'qs';
import store from './vuex/store' //引入store

Vue.prototype.$axios = axios
Vue.prototype.$qs = qs;

//引入自定义的css样式
import './common/stylus/index.css';

Vue.config.productionTip = false
Vue.use(VueRouter);

const routes = [{
		path: '/goods',
		component: goods
	},
	{
		path: '/ratings',
		component: ratings
	},
	{
		path: '/seller',
		component: seller
	},
	{
		path: '/detail',
		component: detail
	}
];

const router = new VueRouter({
	routes, // （缩写）相当于 routes: routes
	linkActiveClass: 'active'
});
// 页面加载自动导航到goods页面
router.push('/goods');

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	store, //相当store,注册后，子组件中可以使用this,store
	components: {
		App
	},
	template: '<App/>'
})