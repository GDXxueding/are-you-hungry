import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
Vue.prototype.$axios = axios

const store = new Vuex.Store({
	state: { //this.$store.state//3
		sellerList: []
	},
//	getters: {
//		increment: function(state) {
//			return state.count++
//		}
//
//	},
	mutations: {//2
		//mutations能达到修改state的值
		SELLERLIST(state, data) {
			state.sellerList = data
		}
	},
	actions: {//1，先请求数据，发布到mutations中执行state赋值
		getSellerJson({commit}) {
			return new Promise((resolve, reject) => {//因为在异步
				axios.get('http://localhost:8081/api/seller').then(res => {
					commit("SELLERLIST", res.data.data)//commit回调给mutations
					resolve()
				});
			})
		}
	}
})
export default store